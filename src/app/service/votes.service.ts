import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResultVote } from './ResultVote';

@Injectable({
  providedIn: 'root'
})
export class VotesService {

  // URL which returns list of JSON items (API end-point URL)
  private readonly URL = 'http://3.235.155.21:3001';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'content-type': 'application/json'
    })
  };

  // create a method named: resolveItems()
  // this method returns list-of-items in form of Observable
  // every HTTTP call returns Observable object
  votesResult(): Observable<ResultVote> {
    console.log('Request is sent!');
    // this.http is a HttpClient library provide by @angular/common
    // we are calling .get() method over this.http object
    // this .get() method takes URL to call API
    return this.http.get<ResultVote>(this.URL + '/api/votes/result');
  }
}
