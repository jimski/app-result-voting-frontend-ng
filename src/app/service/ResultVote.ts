export class ResultVote {
    percentCat: string;
    percentDog: string;
    totalVotes: number;
}