import { Component } from '@angular/core';
import { VotesService } from './service/votes.service';
import { Title } from '@angular/platform-browser';
import { ResultVote } from './service/ResultVote';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Cats vs Dogs -- Result';

  resultVotes:ResultVote;
  percentCat:string;
  percentDog:string;
  total:number = 0;

  public constructor(private titleService: Title, private votesService: VotesService) {
    this.setTitle(this.title);
  }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }
  
  observable: Observable<ResultVote>
  latestValue: number;
  subscription: Subscription
  ngOnInit() {
    //Manual
    this.votesService.votesResult().subscribe((data: {}) => {
      console.log(data);
      this.resultVotes = data as ResultVote;
      this.percentCat = this.resultVotes.percentCat;
      this.percentDog = this.resultVotes.percentDog;
      this.total = this.resultVotes.totalVotes;
    });

    //Check real time
    this.observable = Observable.create(observer => {
      let voteResult
      const interval = setInterval(() => {
        observer.next(voteResult)
        this.votesService.votesResult().subscribe((data: {}) => {
            console.log(data);
            this.resultVotes = data as ResultVote;
            this.percentCat = this.resultVotes.percentCat;
            this.percentDog = this.resultVotes.percentDog;
            this.total = this.resultVotes.totalVotes;
        });
      }, 1000)

      return () => clearInterval(interval)
    });

    // Make sure to save a reference to subscription to
    // be able to unsubscribe later
    this.subscription = this.observable.subscribe();

    // this.observable = Observable.create(observer => {
    //   let value = 0
    //   const interval = setInterval(() => {
    //     observer.next(value)
    //     value++
    //   }, 1000)

    //   return () => clearInterval(interval)
    // });

    // this.subscription = this.observable.subscribe(value => this.latestValue = value);
  }

  ngOnDestroy() {
    // Unsubscribe when the component is destroyed
    this.subscription.unsubscribe()
  }
}
